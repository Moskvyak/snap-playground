import * as React from 'react';
import { createProjectCircle, createTimeslots } from '../utils/radarUtils'

export interface Project {
    id: string
    duration: number
}
interface IProps {
    numberOfCircles: number
    currentTimeslot: number,
    projects: Project[]
}

interface IState {
    componentId: string;
}
export class SnapComponent extends React.Component<IProps, IState> {
    public state: IState = {
        componentId: 'radar'
    }
    
    public componentDidMount() {        
        createTimeslots(`#${this.state.componentId}`, this.props.numberOfCircles);
        createProjectCircle(`#${this.state.componentId}`, this.props.projects);
    }
    
    public componentDidUpdate() {        
        createTimeslots(`#${this.state.componentId}`, this.props.numberOfCircles);
        createProjectCircle(`#${this.state.componentId}`, this.props.projects);
    }

    public render() {
        return <svg id={this.state.componentId} width={'100%'} height={'100%'}/>
    }
}