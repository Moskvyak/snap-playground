import * as React from 'react';
import './App.css';
import { SnapComponent, Project } from './components/SnapComponent';
import logo from './logo.svg';

interface IState {
  numberOfCircles: number;
  currentTimeslot: string;
  projects: Project[];
}
class App extends React.Component<{}, IState> {
  public state = {
    numberOfCircles: 4,
    currentTimeslot: '',
    projects: [
      {
        id: 'blabla',
        duration: 2
      },
      {
        id: 'adga',
        duration: 3
      }
    ]
  };

  public render() {
    const timeslot: number = parseInt(this.state.currentTimeslot, undefined);
    const currentTimeslot = isNaN(timeslot) ? 0 : timeslot;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.tsx</code> and save to reload.
        </p>
        <div style={{ height: 500 }}>
          <SnapComponent
            numberOfCircles={this.state.numberOfCircles}
            currentTimeslot={currentTimeslot}
            projects={this.state.projects}
          />
        </div>
        <button onClick={this.handleClickIncrease}>Increase </button>
        <button onClick={this.handleClickDecrease}>Decrease </button>
        <input
          type="number"
          onChange={this.handleChange}
          value={this.state.currentTimeslot}
        />
        <button onClick={this.addProject}>Add Project </button>  
      </div>
    );
  }

  private addProject = () => {
    const projects = this.state.projects
    projects.push({
      id: `sadfsadf${this.state.projects.length}`,
      duration: this.state.projects.length
    })
    this.setState({projects})
  }
  private handleChange = (e: any) => {
    this.setState({ currentTimeslot: e.target.value });
  };
  private handleClickIncrease = () => {
    this.setState(prevState => {
      return {
        numberOfCircles: prevState.numberOfCircles + 1
      };
    });
  };
  private handleClickDecrease = () => {
    this.setState(prevState => {
      return {
        numberOfCircles: prevState.numberOfCircles - 1
      };
    });
  };
}

export default App;
