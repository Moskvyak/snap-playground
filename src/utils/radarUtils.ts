// @ts-ignore
import Snap from 'snapsvg-cjs';
import { Project } from '../components/SnapComponent'
const padding = 20;
const centerX = 250;
const centerY = 250;
export const createTimeslots = (id: string, numberOfCircles: number) => {
    const s = Snap(id);
   
    const snapchildren = s.children();    
    snapchildren.forEach((snapElement: any) => {        
        if (snapElement.type === 'circle') {
            snapElement.remove();
        }
    })
    for (let i = 0; i< numberOfCircles; i++) {
        const cb = s.circle(centerX, centerY, padding * i + 1);
        cb.attr({ fill: "none", stroke: "red", timeslot: i });
    }
}

export const createProjectCircle = (id: string, projects: Project[]) => {   
    const s = Snap(id);
    const snapchildren = s.children();
    projects.forEach((project: Project) => {
        snapchildren.forEach((snapElement: any) => {
            if (snapElement.type === 'circle') {           
                if (snapElement.attr('projectId') === project.id) {
                    snapElement.remove();
                }
            }
        })
    });
    projects.forEach((project: Project)=> {
        let radius = 1;
        snapchildren.forEach((snapElement: any) => {
            if (snapElement.type === 'circle') {           
                if (snapElement.attr('timeslot') === project.duration.toString()) {
                    radius = parseInt(snapElement.attr('r'), undefined);
                }
            }
        })
        const cb = s.circle(centerX + radius, centerY, 10);
        cb.attr({ fill: "blue", stroke: "green", projectId: project.id});
        console.log(project, radius)
    } )

    
}